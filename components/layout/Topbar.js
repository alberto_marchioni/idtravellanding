import { Nav } from "react-bootstrap";
export default function Topbar() {
  return (
    <>
      <Nav className='justify-content-end mx-3' navbar>
        <Nav.Link href='#features' className='h5'>
          Login
        </Nav.Link>
      </Nav>
    </>
  );
}
