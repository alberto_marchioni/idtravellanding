export default function Footer() {
  return (
    <div
      className='d-flex justify-content-center flex-column'
      style={{ background: "#646473", color: "white" }}
    >
      <a
        href='https://www.idshooter.com'
        target='_blank'
        rel='noopener noreferrer'
        className='text-center py-3'
      >
        <img src='/images/logo_idsh.png' alt='IDShooter Logo' width='80' />
      </a>
      <h6 className='text-center'>
        IDshooter Srl - Via F. Perotti, 5 - 25126 Brescia (BS) - Italy - P.I.
        03626020980 - Tel.: +39 030 2071889 -
      </h6>
    </div>
  );
}
