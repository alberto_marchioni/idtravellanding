import Head from "next/head";
import styles from "../styles/Home.module.css";

import { Jumbotron, Col } from "react-bootstrap";
import Footer from "../components/layout/Footer";
import Topbar from "../components/layout/Topbar";

export default function Home() {
  return (
    <div className={styles.main}>
      <Head>
        <title>IDTravel Landing Page</title>
        <link rel='icon' href='/favicon.ico' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
      </Head>

      <Jumbotron
        fluid
        style={{
          backgroundImage: "url(/images/main.jpg)",
          backgroundPosition: "right top",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "100vw",
          maxHeight: "500px",
        }}
      >
        <Topbar />
        <Col
          sm={{ span: 6, offset: 1 }}
          lg={{ span: 4, offset: 1 }}
          className='px-5 p-md-0'
        >
          <img src='/images/IDTRLogo.svg' alt='IDTravel Logo' width='100%' />
        </Col>
      </Jumbotron>
      <main className={styles.container}>
        <Col md={10} lg={8}>
          <p className={styles.description}>
            <span className='h1'>Cercando </span>il termine “viaggio “ si trova
            una prima definizione: «l'azione del muoversi per andare da un luogo
            all’altro» poi ne segue una più specifica, si potrebbe dire più
            moderna: «giro attraverso luoghi o paesi diversi dal proprio, per
            vedere, conoscere, imparare, sviluppare particolari rapporti
            d'affari, o semplicemente per divertirsi
          </p>
        </Col>
        <div className={styles.grid}>
          <a href='https://nextjs.org/docs' className={styles.card}>
            <h3>Documentation &rarr;</h3>
            <p>Find in-depth information about Next.js features and API.</p>
          </a>

          <a href='https://nextjs.org/learn' className={styles.card}>
            <h3>Learn &rarr;</h3>
            <p>Learn about Next.js in an interactive course with quizzes!</p>
          </a>

          <a
            href='https://github.com/vercel/next.js/tree/master/examples'
            className={styles.card}
          >
            <h3>Examples &rarr;</h3>
            <p>Discover and deploy boilerplate example Next.js projects.</p>
          </a>

          <a
            href='https://vercel.com/import?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app'
            className={styles.card}
          >
            <h3>Deploy &rarr;</h3>
            <p>
              Instantly deploy your Next.js site to a public URL with Vercel.
            </p>
          </a>
        </div>
      </main>

      <Footer />
    </div>
  );
}
